class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_MAX_NUM = 4

  def index
    @taxonomies = Spree::Taxonomy.includes(:root)
    @products = Spree::Product.includes(master: [:default_price])
  end

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.related_products(@product).limit(RELATED_PRODUCTS_MAX_NUM)
  end
end
