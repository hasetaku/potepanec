module Spree::ProductDecorator
  def self.prepended(base)
    base.scope :related_products, -> (product) do
      in_taxons(product.taxons).includes(master: [:images, :default_price]).distinct.where.not(id: product.id).order(updated_at: :desc)
    end
  end
  Spree::Product.prepend self
end
