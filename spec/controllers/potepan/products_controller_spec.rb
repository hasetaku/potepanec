require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:category_taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon) { create(:taxon, taxonomy: category_taxonomy) }
  let(:rails_product) { create(:product, name: "Rails Application", taxons: [taxon]) }

  describe "GET #index" do
    let!(:brand_taxonomy) { create(:taxonomy) }
    let!(:python_product) { create(:product, name: "Python Application", taxons: [taxon]) }

    before do
      get :index
    end

    it "has correct array list of taxonomy variables" do
      expect(assigns(:taxonomies)).to contain_exactly(category_taxonomy, brand_taxonomy)
    end

    it "has correct array list of product variables" do
      expect(assigns(:products)).to contain_exactly(rails_product, python_product)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "renders show.html.erb" do
      expect(response).to render_template(:index)
    end
  end

  describe "GET #show" do
    let!(:related_products) { create_list(:product, 10, taxons: [taxon]) }
    let(:different_taxonomy) { create(:taxonomy, name: "Brand") }
    let(:different_taxon) { create(:taxon, taxonomy: different_taxonomy) }
    let!(:unrelated_product) { create(:product, name: "Unrelated Product", taxons: [different_taxon]) }

    context "Correct http request to show action renders its view" do
      before do
        get :show, params: { id: rails_product.id }
      end

      it "has correct product object" do
        expect(assigns(:product)).to eq rails_product
      end

      it "contains 4 related products" do
        expect(assigns(:related_products).size).to eq 4
      end

      it "not contains unrelated product" do
        expect(assigns(:related_products)).not_to include(unrelated_product)
      end

      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it "renders show.html.erb" do
        expect(response).to render_template :show
      end
    end
  end
end
