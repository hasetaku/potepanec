require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:category_taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon) { create(:taxon, taxonomy: category_taxonomy) }

  describe "GET #show with certain taxon" do
    before do
      get :show, params: { id: taxon.id }
    end

    it "has correct array list of taxonomy variables" do
      brand_taxonomy = create(:taxonomy)
      expect(assigns(:taxonomies)).to contain_exactly(category_taxonomy, brand_taxonomy)
    end

    it "has correct taxon variable" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "has correct array list of product variables" do
      rails_product = create(:product, name: "Ruby Application", taxons: [taxon])
      python_product = create(:product, name: "Python Application", taxons: [taxon])
      expect(assigns(:products)).to contain_exactly(rails_product, python_product)
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "renders show.html.erb" do
      expect(response).to render_template(:show)
    end
  end
end
