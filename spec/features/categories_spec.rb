require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:rails_taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:python_taxon) { create(:taxon, name: "Python", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:rails_product) { create(:product, name: "Rails Application", taxons: [rails_taxon]) }
  let!(:python_product) { create(:product, name: "Python Application", taxons: [python_taxon], price: 777) }

  before do
    visit(potepan_category_path(rails_taxon.id))
  end

  scenario "Visit categories/show.html.erb twice to make sure only expected product is shown" do
    expect(page).to have_title "Ruby on Rails"

    within('div.page-title') do
      expect(page).to have_content rails_taxon.name
    end

    within('#displayedProducts') do
      expect(page).to have_content rails_product.name
      expect(page).to have_content rails_product.display_price
      expect(page).not_to have_content python_product.name
      expect(page).not_to have_content python_product.display_price
    end

    within '.taxonomy_list' do
      expect(page).to have_content taxonomy.name
      click_link python_taxon.name
    end
    expect(page).to have_title python_taxon.name

    within('div.page-title') do
      expect(page).to have_content python_taxon.name
    end

    within('#displayedProducts') do
      expect(page).to have_content python_product.name
      expect(page).to have_content python_product.display_price
      expect(page).not_to have_content rails_product.name
      expect(page).not_to have_content rails_product.display_price
    end
    find('div.productCaption').click
  end
end
