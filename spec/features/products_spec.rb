require 'rails_helper'
RSpec.feature "Products", type: :feature do
  let!(:taxon) { create(:taxon) }
  let!(:java_product) { create(:product, name: "This is JavaApp", taxons: [taxon]) }
  let!(:swift_product) { create(:product, name: "This is SwiftApp", taxons: [taxon]) }

  scenario "Visit products/index.html.erb from top page" do
    visit(potepan_path)
    click_link 'Search'
    expect(page).to have_title "All Products"

    within('div.page-title') do
      expect(page).to have_content "ALL PRODUCTS"
    end

    within('#displayedProducts') do
      expect(page).to have_content java_product.name
      expect(page).to have_content java_product.display_price
      expect(page).to have_content swift_product.name
      expect(page).to have_content swift_product.display_price
    end
  end

  # click_linkするclass="productImage"が２つの商品で重複するため、上記のテストとは分けて実施
  scenario "Visit products/show.html.erb" do
    # product/show.htmlへのクリック遷移テストはcategories_specで検証するため、visitメソッドを選択
    visit(potepan_product_path(java_product.id))
    expect(page).to have_title java_product.name

    within('div.page-title') do
      expect(page).to have_content java_product.name
    end

    within('ol.breadcrumb') do
      expect(page).to have_content java_product.name
      expect(page).to have_link 'Home'
    end
    expect(page).to have_link '一覧ページへ戻る'

    within '#product-details' do
      expect(page).to have_content java_product.name
      expect(page).to have_content java_product.display_price
      expect(page).to have_content java_product.description
    end
    expect(page).to have_link 'カートへ入れる'

    within 'div.productsContent' do
      expect(page).to have_content swift_product.name
      expect(page).to have_content swift_product.display_price
    end
  end
end
